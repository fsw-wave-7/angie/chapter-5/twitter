const messages = {
    '200':'sukses',
    '201':'Data berhasil disimpan',
    '422':'Gagal di proses'
}

function successResponse(
    res,
    code,
    data,
    meta = {}
){

res.status(code).json({
    data:data,
    meta:{
        code:200,
        message: messages[code.toString()], 
        ...meta
        }
    })
}

module.exports = {
    successResponse
}