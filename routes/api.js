const express = require('express')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const Post = require('../controllers/post.js')
const post = new Post 

const User = require('../controllers/user.js')
const user = new User 

const logger = (req,res,next) => {
    console.log(`LOG: ${req.method} ${req.url}`);
    next()
}

const api = express.Router()

api.use(logger)
api.use(jsonParser)

api.get('/post', post.getPost)
api.get('/post/:index', post.getDetailPost)
api.post('/post/', jsonParser, post.insertPost)
api.put('/post/:index', jsonParser, post.upadatePost)
api.delete('/post:index',post.deletePost)

api.get('/user', user.getUser)
api.get('/user/:index', user.getDetailUser)
api.post('/user/', jsonParser, user.insertUser)
api.put('/user/:index', jsonParser, user.upadateUser)
api.delete('/user:index',user.deleteUser)

module.exports = api