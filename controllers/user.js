const { json } = require("body-parser")
const { successResponse } = require('../helper/response.js')

class User{
    constructor(){
        this.user = [
        {
            'nama_lengkap':'angelin margareta',
            'username':'angie27',
            'password' :'12345',
            'email':'angie@gmail.com'
        }
    ]
    }

    getUser = (req,res) => {
        successResponse(
            res,
            200,
            this.user,
            {total:this.user.length}
        ) 
    }

    getDetailUser = (req,res) => {
        const index = req.params.index
        successResponse(
           res,
           200,
            this.user[index]
       )
    }

    insertUser = (req,res) => {
        const body = req.body
        const param = {
            'username' : body.username,
            'password' : body.password,
            'email' : body.email
        }
        this.user.push(param)
        successResponse(
            res,
            201,
            param
        )
    }

    upadateUser = (req,res) => {
        const index = req.params.index
        const body = req.body

        this.user[index].username = body.username,
        this.user[index].password = body.password,
        this.user[index].email = body.email,

        successResponse(
            res,
            200,
            this.user[index]
        )
    }

    deleteUser = (req,res) => {
        const index = req.params.index

        this.user.splice(index,1);
        successResponse(
            res,
            200,
            null
        )
}
}

module.exports = User