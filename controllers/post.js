const { json } = require("body-parser")
const { successResponse } = require('../helper/response.js');
const  fs = require('fs')

class Post{
    constructor(){
        this.post = [];
        const readPost = fs.readFileSync('./data/post.json','utf-8');
        if (readPost == ""){
            this.parsedPost = this.post;
        }else{
            this.parsedPost = JSON.parse(readPost)
        }
    }

    getPost = (req,res) => {
        successResponse(
            res,
            200,
            this.parsedPost,
            {total:this.parsedPost.length}
        ) 
    }

    getDetailPost = (req,res) => {
        const index = req.params.index
        successResponse(
           res,
           200,
            parsedPost[index]
       )
    }

    insertPost = (req,res) => {
        const body = req.body
        const param = {
            'text': body.text,
            'created_at': new Date(),
            'username' : body.username
        }
        this.parsedPost.push(param)
        fs.writeFileSync('./data/post.json',JSON.stringify(this.parsedPost))
        successResponse(
            res,
            201,
            param
        )
    }

    upadatePost = (req,res) => {
        const index = req.params.index
        const body = req.body

        if (this.parsedPost[index]){
        this.parsedPost[index].text = body.text
        this.parsedPost[index].username = body.username
        this.parsedPost[index].created_at = body.created_at
        fs.writeFileSync('./data/post.json',JSON.stringify(this.parsedPost));
        successResponse(
            res,
            200,
            parsedPost[index]
        )}else{
        successResponse(
            res,
            422,
            null
        )}
    }

    deletePost = (req,res) => {
        const index = req.params.index
        this.parsedPost.splice(index,1);
        fs.writeFileSync('./data/post.json',JSON.stringify(this.parsedPost));
        successResponse(
            res,
            200,
            null
        )
}
}

module.exports = Post

//create,read,update,delete