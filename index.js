const express = require('express');
const morgan = require('morgan')
const path = require('path');

const api = require('./routes/api.js')

const app = express()
const port = 3000

app.use('/api', api)
//set view engine
app.set('view engine','ejs')

//load static file
app.use(express.static(__dirname + '/public'))

//3rd party middle ware for logging
app.use(morgan('dev'))

//load api route

app.get('/menu', function(req,res){
    res.render(path.join(__dirname, './views/menu'))
})

app.get('/', function(req,res){
    const name = req.query.name || 'Starbucks'
    res.render(path.join(__dirname, './views/index'),{
        name : name
    })
})

// app.get('/', function(req,res){
//     res.json({
//         'hello': world
//     })
// })

// app.get('/', function(req,res){
//     res.json({
//         'check':'log'
//     })
// })

app.use(function(err,req,res,next){
    res.status(500).json({
        status:'fail',
        errors: err.message
    })
})

app.use(function(req,res,next){
    res.status(404).json({
        status:'fail',
        errors: 'Are you lost?'
    })
})

app.listen(port,() => console.log("Server Berhasil Dijalankan"))